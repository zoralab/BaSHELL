# Contribute to Resources
Thank you for your interest in contributing to into this repository. This guide
details how to contribute this repository efficiently.

<br/>
## Code of Conducts
As contributors and maintainers of this project, we pledge to respect all
people who contribute through reporting issues, posting feature requests,
updating documentation, submitting pull requests or patches, and other
activities. We are committed to making participation in this project a
harassment-free experience for everyone, regardless of level of experience,
gender, gender, identity and expression, sexual orientation, disability,
personal appearance, body size, race, ethnicity, age, or religion.

<br/>
Examples of unacceptable behavior by participants include the use of sexual
language or imagery, derogatory comments or personal attacks, trolling, public
or private harassment, insults, or other unprofessional conduct.

<br/>
Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct. Project maintainers who do not
follow the Code of Conduct may be removed from the project team. This code of
conduct applies both within project spaces and in public spaces when an
individual is representing the project or its community.

<br/>
Instances of abusive, harassing, or otherwise unacceptable behavior can be
reported by emailing to [tech@zoralab.com](mailto:tech@zoralab.com)

This Code of Conduct is adapted from the Contributor Covenant, version 1.1.0,
available at http://contributor-covenant.org/version/1/1/0/.

<br/>
## Agreement
By submitting code as an individual, you're automatically agreed to our
[individual contributor license agreement](https://gitlab.com/ZORALab/Resources/blob/master/contributing/individual_agreement.md).

By submitting code as an entity, you're automatically agreed to our
[corporate contributor license agreement](https://gitlab.com/ZORALab/Resources/blob/master/contributing/corporate_agreement.md).

<br/>
## Steps
### Step 1: Always file an issue
Found a bug? has a new idea? got a question? Always file an issue on the
[Issue Tracker](https://gitlab.com/ZORALab/BaSHELL/issues) and label them as
the ~"discussion" label. In case you know who is the maintainer, you can tag
him into the issue as well.

<br/>
Normally we'll review the values of efforts, sharing and understanding the idea
and by making the discussion open, you can attract like-minded individuals to
contribute together too!

<br/>
### Step 2: Setting up
By default, this repository is a continuous integration software development.
Once the idea is favored to go and you're ready for code contribution, label
the issue with ~"Doing" label. You can proceed by forking the repository under
your account.

<br/>
If GitLab CI is supported in this repository, please do proceed to setup
the necessary environment, such as
[installing the runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner).
Normally, the CI will handles the testing automation for you unless your
involved in its code development. Hence, you just need to focus on developing
your codes and test scripts.


<br/>
### Step 3: Checkout next branch
The master branch is a protected branch. Since this is your forked repository,
you can proceed to develop the solution onto the `next` branch. You can proceed
to clone your forked repository and checkout:
```
$ git clone -b next <GIT_URL>
```

You may then proceed with your development. Please do keep in mind to update 
your progress and notify your co-author too.


> NOTE:
>
> If you are a team, try to branch out and eventually, have all the commit
> merges into `next` branch for a single pull request. This simplifies the
> processes and makes thing easier for you to manage your team.

<br/>
### Step 4: Development
We do not impose any strict regulations for the `commmit messages` but do we do
have some expectations too. Example:

#### 1: Tag your commit with the issue number
Since you have an issue tracker with you, tag it in your commit title. Example,
say your issue number is 12,
```
Feature #12 - <commit title>

...
```
This helps us to keep track which codes tied to which commitment and greatly
help in root-cause analysis during bug fixing.

> NOTE:
>
> The tag follows your issue main tag. Example:
>
> 1. **documentation** is for ~"Documentation" tag
> 2. **issue** is for ~"bug" tag
> 3. **feature** is for ~"new feature" and ~"enhancement" tag
> 4. **ticket** is for any other tag not found in the above.
>
> In case you wish to double-confirm the appropriate tag, just go ahead and ask
> in the your issue-tracker message section.

#### 2. Keep your commit title and message width to 80 characters max
Most of us are using terminals to view the commits and codes, please do keep
it short. You can keep the details in the message section or inside your
issue tracker.

Example:
```
Feature #12 - adding feature A into repository

This patch is created to add feature A, that is capable of doing XYZ to 
eliminate redundancy. It enables user to do XYZ too.
```

#### 3. Keep your commit small if possible
Try to commit small and often. Please avoid 1 big commit if possible. You'll
scare other developers away and probably, your code reviewers may want to 
review your code either. Just like in note number 2, keep it simple.

> TIPS:
>
> if you're using `and` in your commit title, chances are, you're commiting big.
> Example:
> ```
> Feature #12 - add feature A and feature B into repository
> ```
>
> This can be done by:
> ```
> Feature #12 - add feature A into repository
> ```
>
> ```
> Feature #12 - add feature B into repository
> ```

#### 4. Make sure you push your code to your next branch
This is to leverage the CI integration that is available for you. We set our 
CI to run the test at each commit delivery. Hence, please do make sure your
next branch is cleared from failure.

> **The Definition of `Done`**:
>
> When we said `Done`, we meant by:
> 1. new codes development is completed **AND**
> 2. test script is completed and tested when applicable **AND**
> 3. documentations are updated when applicable **AND**
> 4. changelog is drafted. You need to draft out the messages for the changelog.

<br/>
### Step 5: Raise merge request
Once you're ready, raise a `merge request`. There are a few things to make
sure it works:
1. Ensure you merge into the original repository's `next` branch, not `master`.
2. Make sure you describe and link the Issue Tracker in your URL.
3. Save the merge request URL into Issue tracker for tracking.

Have the CI runs the test and have the reviewers review the code for merging.
Once confirmed and tests are running smoothly, you can proceed with the merging.

These are the critical things you need to do in your issue tracker:
1. have the drafted `changelog` details mentioned in your issue tracker.

> NOTE:
>
> Don't worry, the maintainer will add the details into the changelog prior
> to releasing to `master` branch. 


<br/>
### Step 6: Clear and remove your forked repository
That's all for committing your codes into the repository. Depending on your
choice, we recommend you to remove the forked repository until the next
development.

<br/>
## Styles and Coding Guidelines
There are various coding languages and documentations in this repository.
Please follow the following guidelines appropriately.

### Secrets
> **IMPORTANT NOTE**:
> 
> DO NOT post your secrets onto the repository, not even `committing` it to
> your local git repo. It's very hard to clean those details after `commit` and
> is very dangerous since it exposes the secrets to the open public, sometimes
> can incur personal losses is not limited to financial aspect too.

Use separate source file or environment variables if possible. If you're using
separate source file, remember to add the filename into the `.gitignore`.

### Coding Guidelines
You should fully respect the coding guidelines mentioned in the README.md.